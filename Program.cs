﻿
using System.Drawing;
using Steganography;

if (args.Length < 1)
{
    Console.WriteLine("Usage:");
    Console.WriteLine("Encode text in image: \tSteganography ./myBitmap.png \"My secret text\"");
    Console.WriteLine("Extract text from image: \tSteganography ./myBitmapWithSecret.png");
    return;
}

if (args.Length == 1)
{
    Console.WriteLine("Passed one file, trying to decode an image...");
    var bitmap = LoadBitmap(args[0]);
    if (bitmap == null) return;

    var text = Coder.ExtractFromBitmap(bitmap);
    Console.WriteLine("Text from image:");
    Console.WriteLine(text);
}

if (args.Length == 2)
{
    Console.WriteLine("Passed two files, trying to encode an image...");
    var bitmap = LoadBitmap(args[0]);
    if (bitmap == null) return;
    var text = args[1];
    var newBitmap = Coder.HideInBitmap(text, bitmap);
    newBitmap.Save("textInImage.png");
}


Bitmap? LoadBitmap(string path)
{
    Bitmap? bmp = null;
    try
    {
        bmp = new Bitmap(path);
    }
    catch (Exception e)
    {
        Console.WriteLine("Error: " + e.Message);
    }
    return bmp;
}