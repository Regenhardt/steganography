﻿namespace Steganography;

using System.Drawing;

internal class Coder
{
    static bool end;
    public static Bitmap HideInBitmap(string text, Bitmap bmp)
    {
        end = false;
        int charValue = 0;
        int zeros = 0;// add bit 0 to indicate end of insert
        int charIndex = 0;//position of hidden character in mess
        long pixelIndex = 0;// save the pixel position under consideration in the image
        int R = 0, G = 0, B = 0;
        for (int i = 0; i < bmp.Height; i++)
        {
            for (int j = 0; j < bmp.Width; j++)
            {
                Color pixel = bmp.GetPixel(j, i);// create temporary variable to store pixel value at position (j,i)
                R = pixel.R - pixel.R % 2;// clean the LSB bits at the end without inserting these bits will carry the value 0
                G = pixel.G - pixel.G % 2;
                B = pixel.B - pixel.B % 2;
                for (int n = 0; n < 3; n++) //iterate through each pixel element R,G,B
                {
                    if (pixelIndex % 8 == 0) //If you switch to a new character to process, each character needs to insert 8bit into 8 color bytes, so when you switch to inserting a new character, it will start from pixel%8=0.
                    {
                        if (end = true && zeros == 8)// When the message runs out, hide and increase zeros se = 8 -> end
                        {
                            if (((pixelIndex - 1) % 3) < 2)
                                bmp.SetPixel(j, i, Color.FromArgb(R, G, B));// update value
                            return bmp;
                        }
                        if (charIndex >= text.Length)
                            end = true;        // when the message needs to be inserted, set end=true otherwise, move on to the next character to start inserting;
                        else charValue = text[charIndex++];
                    }
                    switch (pixelIndex % 3)         // change color bytes if
                    {
                        case 0:                     // The color byte R- has a remainder divided by 3 =0
                            if (end == false)
                            {
                                R += charValue % 2;
                                charValue /= 2;
                            }
                            break;
                        case 1:                     // color byte G- has a remainder divided by 3 =1
                            if (end == false)
                            {
                                G += charValue % 2;
                                charValue /= 2;
                            }
                            break;
                        case 2:                     // color byte B- has a remainder divided by 3 =2
                            if (end == false)
                            {
                                B += charValue % 2;
                                charValue /= 2;
                            }
                            bmp.SetPixel(j, i, Color.FromArgb(R, G, B));// reset R/G/B color value after re-changing
                            break;
                    }
                    pixelIndex++;
                    if (end)// when the message has finished inserting, increment zeros to 8
                        zeros++;
                }
            }
        }
        return bmp;
    }

    public static string ExtractFromBitmap(Bitmap bmp)
    {
        string text = string.Empty;
        int charValue = 0;                               // the ascii code of each character in the message to be extracted
        long colorUnitIndex = 0;                    //  To get 8 bits from a pixel, r computes the output character
        for (int i = 0; i < bmp.Height; i++)
        {
            for (int j = 0; j < bmp.Width; j++)
            {
                Color pixel = bmp.GetPixel(j, i);           // get pixel at position (j,i)
                for (int n = 0; n < 3; n++)             // loop to get the turn R G
                {
                    switch (colorUnitIndex % 3)
                    {
                        case 0:
                            charValue = charValue * 2 + pixel.R % 2;              // take LSB of R
                            break;
                        case 1:
                            charValue = charValue * 2 + pixel.G % 2;
                            break;
                        case 2:
                            charValue = charValue * 2 + pixel.B % 2;
                            break;
                    }
                    colorUnitIndex++;
                    if (colorUnitIndex % 8 == 0)        // 8 bits, then convert to
                    {
                        charValue = ReverseBits(charValue);       // the process of inserting is opposite to getting, so change the bit in the opposite direction
                        if (charValue == 0)                  // when char has value 8bit 0-> insert message has expired, we return the found message
                            return text;
                        char c = (char)charValue;
                        text += c;
                    }
                }
            }
        }
        return text;
    }

    public static int ReverseBits(int n)
    {
        int result = 0;

        for (int i = 0; i < 8; i++)
        {
            result = result * 2 + n % 2;

            n /= 2;
        }

        return result;
    }
}
