# Steganography

Testing a program written by someone on reddit.

Seems to work so far, I basically just translated stuff so I could understand it:

![](Screenshot.png)

Original image:

![](Landscape.png)

Image with text "Multi word test" encoded in it:

![](textInImage.png)